module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: '\r\n',
      },
      app: {
        src: [
          'src/js/**/*'
        ],
        dest: 'build/js/app.js'
      },
      libs_styles: {
        src: ['libs/bootstrap/dist/css/bootstrap.min.css'],
        dest: 'build/css/<%= pkg.name %>.libs.css'
      },
      libs_js: {
        src: ['libs/jquery/dist/jquery.min.js', 'libs/bootstrap/dist/js/bootstrap.min.js', 'libs/Namespace.js/Namespace.js'],
        dest: 'build/js/<%= pkg.name %>.libs.js',
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      libs: {
        files: {
          'build/js/<%= pkg.name %>.libs.min.js': ['build/js/<%= pkg.name %>.libs.js']
        }
      },
      all: {
        files: {
          'build/js/<%= pkg.name %>.min.js': ['build/js/<%= pkg.name %>.libs.js', 'build/js/app.js']
        }
      }
    },
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "build/css/main.css": ["src/css/main.less"]
        }
      }
    },
    cssmin: {
      target: {
        files: {
          'build/css/main.min.css': ['build/css/roiback_test.libs.css', 'build/css/main.css'],
        }
      }
    },
    copy: {
      files: {
        cwd: 'libs/bootstrap/dist/fonts', // set working folder / root to copy
        src: '**/*', // copy all files and subfolders
        dest: 'build/fonts', // destination folder
        expand: true // required when using cwd
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');

  // Default task(s).
  grunt.registerTask('default', ['concat:app', 'concat:libs_styles', 'concat:libs_js', 'uglify:libs', 'uglify:all', 'less', 'cssmin', 'copy']);

};